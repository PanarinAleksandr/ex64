<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Words;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadWordData extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $word = new Words();
        $word -> translate('ru')->setWord('партк');
        $word -> translate('en')->setWord('parta');
        $word -> translate('jp')->setWord('jp1');
        $word -> translate('gm')->setWord('gm1');
        $word -> translate('it')->setWord('it1');
        $word->mergeNewTranslations();
        $manager->persist($word);

        $manager->flush();
    }
}
