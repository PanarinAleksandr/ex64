<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadUserData extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $pass = password_hash('ewq', PASSWORD_DEFAULT);

        $user = new User();
        $user
            ->setEmail('ewq@ewq.ru')
            ->setUsername('Vova')
            ->setRoles(['ROLE_USER'])
            ->setPassword($pass)
            ->setEnabled(true);
        $manager->persist($user);


        $pass1 = password_hash('zxczxczxc', PASSWORD_DEFAULT);
        $user1 = new User();
        $user1
            ->setEmail('zxc@zxc.ru')
            ->setUsername('Ivan')
            ->setRoles(['ROLE_USER'])
            ->setPassword($pass1)
            ->setEnabled(true);
        $manager->persist($user1);

        $manager->flush();
    }
}
