<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Words;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

class TranslatorController extends Controller
{
    /**
     * @Route("/", name="homePage")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $button = $this->get('translator')->trans('save');
            $form = $this->createFormBuilder()
                ->add('ruWord', TextType::class,['label'=>false, ])
                ->add('save', SubmitType::class,['label'=> $button])
                ->getForm();
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $data = $form->getData();
                $word = new Words();
                $word->translate('ru')->setWord($data['ruWord']);
                $em = $this->getDoctrine()->getManager();
                $em->persist($word);
                $word->mergeNewTranslations();
                $em->flush();
                return $this->redirectToRoute('homePage');
            }

            $words = $this->getDoctrine()->getRepository('AppBundle:Words')->findAll();
            return $this->render('@App/Translator/index.html.twig', [
                'form' => $form->createView(),
                'words'=>$words
            ]);
    }


    /**
     * @Route("/new/{id}", name="new",requirements={"id" : "\d+"})
     * @param Request $request
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request, int $id)
    {
        $button = $this->get('translator')->trans('save');
        $en = $this->get('translator')->trans('transEn');
        $jp = $this->get('translator')->trans('transJp');
        $gp = $this->get('translator')->trans('transGm');
        $it = $this->get('translator')->trans('transIt');

        $form = $this->createFormBuilder()
            ->add('en', TextType::class, ['required'=>false])
            ->add('jp', TextType::class, ['required'=>false])
            ->add('gm', TextType::class, ['required'=>false])
            ->add('it', TextType::class, ['required'=>false])
            ->add('save', SubmitType::class,["label"=>$button])
            ->getForm();

        $form->handleRequest($request);
        $word = $this->getDoctrine()
            ->getRepository('AppBundle:Words')
            ->find($id);


        if ($form->isSubmitted()) {
            $data = $form->getData();

                if ($data['en'] != null) {
                    $word->translate('en', false)->setWord($data['en']);
                }
                if ($data['it'] != null) {

                    $word->translate('it', false)->setWord($data['it']);
                }
                if ($data['gm'] != null) {
                    $word->translate('gm', false)->setWord($data['gm']);
                }
                if ($data['jp'] != null) {
                    $word->translate('jp', false    )->setWord($data['jp']);
                }
                $em = $this->getDoctrine()->getManager();
                    $em->persist($word);
                    $word->mergeNewTranslations();
                    $em->flush();
                    return $this->redirect($this->generateUrl('new',['id' => $id]));
                }

        return $this->render('@App/Translator/new.html.twig', array(
            'form'=>$form -> createView(),
            'words' => $word,
        ));
    }

    /**
     * @Route("/{_locale}", requirements = {"_locale" : "en|ru"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function changeLocalAction(Request $request){

        return $this->redirect($request->server->get('HTTP_REFERER'));
    }

}
