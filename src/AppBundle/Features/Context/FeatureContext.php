<?php

namespace AppBundle\Features\Context;

use Behat\Behat\Tester\Exception\PendingException;
use Behat\Gherkin\Node\TableNode;
use Behat\MinkExtension\Context\MinkContext;
use Behat\Symfony2Extension\Context\KernelAwareContext;
use Symfony\Component\HttpKernel\KernelInterface;


/**
 * Defines application features from the specific context.
 */
class FeatureContext extends MinkContext implements KernelAwareContext
{
    /** @var  KernelInterface */
    private $kernel;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }

    protected function getContainer()
    {
        return $this->kernel->getContainer();
    }

    /**
     * @When /^я нахожусь на главной странице$/
     */
    public function яНахожусьНаГлавнойСтранице()
    {
        $this->visit($this->getContainer()->get('router')->generate('homePage'));
    }

    /**
     * Sets Kernel instance.
     *
     * @param KernelInterface $kernel
     */
    public function setKernel(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }


    /**
     * @When /^я нажимаю на кнопку "([^"]*)" и перехожу на форму авторизации$/
     */
    public function яНажимаюНаКнопкуИПерехожуНаФормуАвторизации($arg1)
    {

        $this->clickLink($arg1);

        $this->visit($this->getContainer()->get('router')->generate('fos_user_security_login'));
    }


    /**
     * @When /^я ввожу данные в форму$/
     */
    public function яВвожуДанныеВФорму()
    {
        $session = $this->getSession();
        $session->getPage()->find('css', 'input#username')->setValue('Vova');
        sleep(1);
        $session->getPage()->find('css', 'input#password')->setValue('ewq');
        sleep(1);
        $session->getPage()->find('css', 'input#_submit')->click();
        sleep(1);
    }


    /**
     * @When /^я авторизипрвался и попадаю на домашнюю страниуц где ввожу дясять слов$/
     * @param TableNode $table
     */
    public function яАвторизипрвалсяИПопадаюНаДомашнююСтраниуцГдеВвожуДясятьСлов(TableNode $table)
    {
        $session = $this->getSession();
        foreach ( $table as  $word) {
               $session->getPage()->find('css', 'input#form_ruWord')->setValue($word['данные']);
               $session->getPage()->find('css', 'button#form_save')->click();
       }
    }

    /**
     * @When /^я  нахожу список слов$/
     * @param TableNode $table
     */
    public function яНахожуСписокСлов1(TableNode $table)
    {
        $session = $this->getSession();
        foreach ( $table as  $word) {
            $session->getPage()->findLink($word['данные'])->click();
            $session->getPage()->find('css','input#form_en')->setValue($word['en']);
            $session->getPage()->find('css','input#form_jp')->setValue($word['jp']);
            $session->getPage()->find('css','input#form_gm')->setValue($word['gm']);
            $session->getPage()->find('css','input#form_it')->setValue($word['it']);
            $session->getPage()->find('css', 'button#form_save')->click();
            sleep(5);
            $session->getPage()->findLink('Home page')->click();
        }

    }


}
