<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity
 */
class WordsTranslation
{
    use ORMBehaviors\Translatable\Translation;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $word;

    /**
     * @param string
     * @return null
     */
    public function setWord($word)
    {
        $this->word = $word;
        return $this;
    }

    /**
     * @return string
     */
    public function getWord()
    {
        return $this->word;
    }

}
